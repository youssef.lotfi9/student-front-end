import types from "../types";

export const createStudentRedux = (student) => {
  return {
    type: types.CREATE_STUDENT,
    payload: student,
  };
};

export const getStudentsRedux = (action) => {
  return {
    type: types.GET_STUDENTS_FETCH,
    payload: {
      page: action?.page,
      pageSize: action?.pageSize,
    },
  };
};

export const deleteStudentByIdRedux = (idStudent) => {
  return {
    type: types.DELETE_STUDENT_BY_ID,
    id: idStudent,
  };
};
export const updateStudentRedux = (student) => {
  return {
    type: types.UPDATE_STUDENT_BY_ID,
    payload: student,
  };
};
export const getStudentRedux = (idStudent) => {
  return {
    type: types.GET_STUDENT,
    payload: idStudent,
  };
};
