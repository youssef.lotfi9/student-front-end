import React, { useEffect } from "react";
import { Container } from "@mui/system";
import { Grid, Snackbar, Stack } from "@mui/material";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import { Button } from "@mui/material";
import { useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import AddModule from "./AddModule";
import ModalUpdate from "./ModalUpdate";
import { StyledTableCell } from "../material/StyledTableCell";
import { openModalSlice } from "../../redux/slice/modalSlice";
import { Alert } from "../material/Alert";
import { unsetError } from "../../redux/slice/studentSlice";
import { Item } from "../material/Item";
import { getStudentRedux } from "../../redux/actions/studentActions";
import { deleteModuleByIdRedux } from "../../redux/actions/moduleActions";
import DeleteIcon from "@mui/icons-material/Delete";
import ModeEditOutlineSharpIcon from "@mui/icons-material/ModeEditOutlineSharp";
function UpdateStudent() {
  const student = useSelector((s) => s.student);
  const { isOpen } = useSelector((s) => s.modal);
  const { idStudent } = useParams();

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getStudentRedux(idStudent));
  }, [idStudent, dispatch, student.error?.message]);

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    dispatch(unsetError());
  };
  return (
    <>
      {student.error?.message ? (
        <Stack spacing={3} sx={{ width: "100%" }}>
          <Snackbar open={true} autoHideDuration={3000}>
            <Alert
              severity="error"
              sx={{ width: "100%" }}
              onClose={handleClose}
            >
              {student.error.message}
              {setTimeout(() => {
                dispatch(unsetError());
              }, 3000)}
            </Alert>
          </Snackbar>
        </Stack>
      ) : null}
      <Container
        maxWidth="lg"
        style={{ backgroundColor: "red", padding: "20px" }}
      >
        <Grid container spacing={2}>
          <Grid item xs={5}>
            <Item>
              <AddModule student={student} />
            </Item>
          </Grid>
          <Grid item xs={10}>
            <Item>
              <TableContainer component={Paper}>
                <Table sx={{ minWidth: 400 }} aria-label="customized table">
                  <TableHead>
                    <TableRow>
                      <StyledTableCell align="center">
                        ID Module
                      </StyledTableCell>
                      <StyledTableCell align="center">
                        Module Name
                      </StyledTableCell>
                      <StyledTableCell align="center">
                        Number of hours
                      </StyledTableCell>
                      <StyledTableCell
                        align="right"
                        style={{ paddingRight: "60px" }}
                      >
                        Actions
                      </StyledTableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {student.modules.map((m) => (
                      <TableRow key={Math.random() * 100}>
                        <TableCell component="th" scope="row">
                          {m.id}
                        </TableCell>
                        <TableCell align="center">{m.name}</TableCell>
                        <TableCell align="center">{m.hours}</TableCell>
                        <TableCell align="right">
                          <DeleteIcon
                            style={{ padding: "5px 5px 0 0" }}
                            onClick={() => {
                              dispatch(deleteModuleByIdRedux(m.id, student.id));
                            }}
                          />
                          <ModeEditOutlineSharpIcon
                            style={{ padding: "5px 25px 0 19px" }}
                            onClick={() => {
                              dispatch(
                                openModalSlice({
                                  idModule: m.id,
                                  idStudent: idStudent,
                                  name: m.name,
                                  hours: m.hours,
                                })
                              );
                              dispatch(unsetError());
                            }}
                          />
                          {isOpen && <ModalUpdate isOpen={isOpen} />}
                          <br />
                        </TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
            </Item>
          </Grid>
        </Grid>
      </Container>
    </>
  );
}

export default UpdateStudent;
