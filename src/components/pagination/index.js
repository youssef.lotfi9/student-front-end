import React, { useEffect, useState } from "react";
import { Box, Pagination } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { getStudentsRedux } from "../../redux/actions/studentActions";

const pageSize = 5;

const AppPagination = () => {
  const [pagination, setPagination] = useState({
    page: 0,
    totalPages: null,
  });
  const students = useSelector((s) => s.students);

  const dispatch = useDispatch();

  useEffect(() => {
    setPagination({
      ...pagination,
      totalPages: students.totalPages,
      page: pagination.page,
    });
    dispatch(getStudentsRedux({ page: pagination.page, pageSize }));
  }, [dispatch, pagination.page, pagination.totalPages]);

  return (
    <Box justifyContent={"center"} alignItems={"center"} display={"flex"}>
      <Pagination
        count={pagination.totalPages}
        onChange={(event, page) => {
          setPagination({
            ...pagination,
            totalPages: students.totalPages,
            page: page - 1,
          });
        }}
      />
    </Box>
  );
};

export default AppPagination;
