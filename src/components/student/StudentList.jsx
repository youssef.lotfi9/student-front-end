import * as React from "react";
import { useDispatch, useSelector } from "react-redux";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import {
  Backdrop,
  Button,
  CircularProgress,
  Snackbar,
  Stack,
  TableFooter,
} from "@mui/material";
import { Link } from "react-router-dom";

import { StyledTableCell } from "../material/StyledTableCell";
import { Alert } from "../material/Alert";
import { deleteStudentByIdRedux } from "../../redux/actions/studentActions";
import AppPagination from "../pagination";

function StudentList({ students }) {
  const dispatch = useDispatch();
  const { isLoading, error } = useSelector((s) => s.students);

  return (
    <>
      <Stack spacing={3} sx={{ width: "100%" }}>
        <Snackbar open={error ? true : false} autoHideDuration={6000}>
          <Alert severity="error" sx={{ width: "100%" }}>
            {error}
          </Alert>
        </Snackbar>
      </Stack>
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 400 }} aria-label="customized table">
          <TableHead>
            <TableRow>
              <StyledTableCell align="center">ID Student</StyledTableCell>
              <StyledTableCell align="center">Full Name</StyledTableCell>
              <StyledTableCell align="center">Branch</StyledTableCell>
              <StyledTableCell align="right" style={{ paddingRight: "60px" }}>
                Actions
              </StyledTableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {isLoading ? (
              <TableRow>
                <Backdrop
                  sx={{
                    color: "#fff",
                    zIndex: (theme) => theme.zIndex.drawer + 1,
                  }}
                  open={isLoading}
                >
                  <CircularProgress color="inherit" />
                </Backdrop>
              </TableRow>
            ) : (
              students.map((student) => (
                <TableRow key={student.id}>
                  <TableCell component="th" scope="row">
                    {student.id}
                  </TableCell>
                  <TableCell align="center">{student.fullName}</TableCell>
                  <TableCell align="center">{student.branch}</TableCell>
                  <TableCell align="right">
                    <Button
                      size="small"
                      style={{ margin: "0px" }}
                      variant="contained"
                      color="error"
                      onClick={() =>
                        dispatch(deleteStudentByIdRedux(student.id))
                      }
                    >
                      Delete
                    </Button>
                    <Link to={`/students/${student.id}/modules`}>
                      <Button
                        size="small"
                        style={{ margin: "0px 0px 0px 8px" }}
                        variant="contained"
                      >
                        Update
                      </Button>
                    </Link>
                  </TableCell>
                </TableRow>
              ))
            )}
          </TableBody>
        </Table>
        <TableFooter
          sx={{
            display: "flex",
            justifyContent: "center",
          }}
        >
          <TableRow>
            <TableCell>
              <AppPagination />
            </TableCell>
          </TableRow>
        </TableFooter>
      </TableContainer>
    </>
  );
}

export default StudentList;
