import { all } from "redux-saga/effects";
import { watchModulesAsync } from "./moduleSaga";
import { watchStudentsAsync } from "./studentSaga";

export function* rootSaga() {
  yield all([watchStudentsAsync(), watchModulesAsync()]);
}
