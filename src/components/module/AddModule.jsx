import React from "react";
import { Container } from "@mui/system";
import { Button, TextField } from "@mui/material";
import { useForm } from "react-hook-form";
import { useDispatch } from "react-redux";
import { createModuleRedux } from "../../redux/actions/moduleActions";

function AddModule({ student }) {
  const dispatch = useDispatch();

  const { register, handleSubmit, reset } = useForm({
    name: "",
    hours: "",
  });

  const onSubmit = (data) => {
    const { name, hours } = data;
    const module = {
      name,
      hours,
    };
    dispatch(createModuleRedux(student.id, module));
    reset({ name: "", hours: "" });
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Container
        sx={{ backgroundColor: "#90cdec", padding: "2%", minWidth: "40px" }}
      >
        <TextField
          fullWidth
          size="small"
          margin="normal"
          id="standard-basic"
          label="Module name"
          variant="outlined"
          {...register("name")}
        />
        <TextField
          type="number"
          fullWidth
          size="small"
          margin="normal"
          id="standard-basic"
          label="number of hours"
          variant="outlined"
          {...register("hours")}
        />

        <div style={{ paddingTop: "10px" }}></div>

        <Button style={{ margin: "10px" }} variant="outlined" type="submit">
          add
        </Button>
      </Container>
    </form>
  );
}

export default AddModule;
