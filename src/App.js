import "./App.css";
import React from "react";
import { Route, Routes } from "react-router-dom";
import AppContainer from "./components/AppContainer";

import MyTabs from "./components/MyTabs";
import Error from "./components/Error";

function App() {
  return (
    <>
      <Routes>
        <Route path="/" element={<AppContainer />} />
        <Route path="/students/:idStudent/modules" element={<MyTabs />} />
        <Route path="*" element={<Error />} />
      </Routes>
    </>
  );
}

export default App;
