import {
  deleteStudentByIdAPI,
  getStudentsAPI,
  createStudentAPI,
  updateStudentAPI,
  getStudentByIdAPI,
} from "../../apis";
import { put, takeEvery } from "redux-saga/effects";
import {
  addStudentFailedSlice,
  addStudentSlice,
  addStudentSuccessSlice,
  deleteStudentFailedSlice,
  deleteStudentSlice,
  deleteStudentSuccessSlice,
  fetchStudentsSlice,
  getStudentsFailedSlice,
  getStudentsSuccessSlice,
} from "../slice/studentsSlice";

import {
  setStudentSlice,
  updateStudentSlice,
  updateStudentSuccessSlice,
  updateStudentFailedSlice,
} from "../slice/studentSlice";

import types from "../types";

export function* getStudentsSaga(action) {
  const { page, pageSize } = action.payload;
  try {
    const response = yield getStudentsAPI(page, pageSize);
    const { totalPages } = response.data;
    yield put(fetchStudentsSlice());
    console.log("Fetching students:", response);
    yield put(
      getStudentsSuccessSlice({
        data: response.data,
        totalPages,
        page: page ? page : 0,
      })
    );
  } catch (e) {
    e.message
      ? yield put(getStudentsFailedSlice(e.message))
      : yield put(getStudentsFailedSlice("error getting students"));
  }
}

export function* deleteStudentByIdSaga(action) {
  try {
    yield deleteStudentByIdAPI(action.id);
    yield put(deleteStudentSlice());
    yield put(deleteStudentSuccessSlice(action.id));
  } catch (error) {
    error.response.data.message
      ? yield put(deleteStudentFailedSlice(error.response.data.message))
      : yield put(
          deleteStudentFailedSlice("error: location: delete Student by id saga")
        );
  }
}

export function* addNewStudentSaga(action) {
  try {
    const { payload } = action;
    const student = {
      id: null,
      fullName: payload.fullName,
      branch: payload.branch,
      modules: [],
    };
    const resp = yield createStudentAPI(student);

    yield put(addStudentSlice());
    yield put(addStudentSuccessSlice({ ...student, id: resp.data.id }));
  } catch (error) {
    console.log(error);
    yield put(addStudentFailedSlice("error : location: addnewStudentSaga"));
  }
}

export function* updateStudentByIdSaga(action) {
  try {
    const { payload } = action;
    const response = yield updateStudentAPI(payload);
    yield put(updateStudentSlice());
    yield put(updateStudentSuccessSlice(response));
  } catch (error) {
    console.log("update student error:", error.response.data.message);
    error.response.data.message
      ? yield put(updateStudentFailedSlice(error.response.data.message))
      : yield put(updateStudentFailedSlice("error update student saga!!"));
  }
}

export function* setStudentSaga(action) {
  try {
    const { payload } = action;
    const { data } = yield getStudentByIdAPI(payload);
    yield put(setStudentSlice(data));
  } catch (error) {
    error.response.data.message
      ? yield put(getStudentsFailedSlice(error.response.data.message))
      : yield put(
          getStudentsFailedSlice(
            "cant fetch student , sorry : location getstudentSaga"
          )
        );
  }
}

export function* watchStudentsAsync() {
  yield takeEvery(types.GET_STUDENTS_FETCH, getStudentsSaga);
  yield takeEvery(types.CREATE_STUDENT, addNewStudentSaga);
  yield takeEvery(types.UPDATE_STUDENT_BY_ID, updateStudentByIdSaga);
  yield takeEvery(types.DELETE_STUDENT_BY_ID, deleteStudentByIdSaga);
  yield takeEvery(types.GET_STUDENT, setStudentSaga);
}
