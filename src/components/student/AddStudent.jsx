import React from "react";
import {
  Button,
  TextField,
  Grid,
  Select,
  InputLabel,
  MenuItem,
  FormControl,
} from "@mui/material";
import { Container } from "@mui/system";
import { useDispatch } from "react-redux";

import { useForm, useController } from "react-hook-form";
import { Item } from "../material/Item";
import { createStudentRedux } from "../../redux/actions/studentActions";

function AddStudent() {
  const dispatch = useDispatch();
  const { register, control, handleSubmit, reset } = useForm({
    fullName: "",
    branch: "",
  });

  const onSubmit = (data) => {
    const { fullName, branch } = data;
    const student = {
      fullName: fullName,
      branch: branch,
      modules: [],
    };
    dispatch(createStudentRedux(student));
    reset({ fullName: "", branch: "" });
  };

  const handleSelectChange = (option) => {
    field.onChange(option.value);
  };

  const { field } = useController({ name: "branch", control });

  const branchOptions = ["TDI", "TRI"];

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Grid
        xs={5}
        padding={1}
        item={true}
        sx={{
          height: "70%",
          padding: "20px 0 20px 0",
          display: "block",
          marginLeft: "auto",
          marginRight: "auto",
          width: "50%",
          boxShadow: "none",
        }}
      >
        <Item>
          <Container
            sx={{ backgroundColor: "#90cdec", padding: "2%", minWidth: "50px" }}
          >
            <TextField
              fullWidth
              size="small"
              margin="normal"
              id="standard-basic"
              label="Student Full name"
              variant="outlined"
              {...register("fullName")}
            />
            <FormControl
              fullWidth
              margin="normal"
              size="small"
              sx={{ minWidth: "70px" }}
            >
              <InputLabel id="demo-simple-select-label" variant="filled">
                Branch
              </InputLabel>
              <Select
                defaultValue={branchOptions[0]}
                {...register("branch")}
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                // value={field.value}
                onChange={handleSelectChange}
              >
                {branchOptions.map((g) => (
                  <MenuItem key={g} value={g}>
                    {g}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>

            <div style={{ paddingTop: "10px" }}></div>

            <Button style={{ margin: "10px" }} variant="outlined" type="submit">
              add
            </Button>
          </Container>
        </Item>
      </Grid>
    </form>
  );
}

export default AddStudent;
