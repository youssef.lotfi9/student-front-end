import { createSlice } from "@reduxjs/toolkit";

const studentSlice = createSlice({
  name: "students",
  initialState: {
    students: [],
    isLoading: true,
    error: null,
    totalPages: 0,
    size: 1,
    page: 0,
  },

  reducers: {
    fetchStudentsSlice: (state) => {
      return {
        ...state,
        isLoading: true,
      };
    },
    getStudentsSuccessSlice: (state, action) => {
      const { data, totalPages, page } = action.payload;
      data.sort(function (a, b) {
        if (a.name < b.name) {
          return -1;
        }
        if (a.name > b.name) {
          return 1;
        }
        return 0;
      });
      return {
        ...state,
        students: data,
        isLoading: false,
        error: null,
        totalPages: totalPages,
        page,
      };
    },

    getStudentsFailedSlice: (state, action) => {
      return {
        ...state,
        isLoading: false,
        students: [],
        error: action.payload,
      };
    },

    addStudentSlice: (state, action) => {
      return { ...state, isLoading: true, error: null };
    },
    addStudentSuccessSlice: (state, action) => {
      const { id, fullName, branch } = action.payload;
      return {
        ...state,
        isLoading: false,
        error: null,
        students: state.students.concat({
          id,
          fullName,
          branch,
          modules: [],
        }),
      };
    },

    addStudentFailedSlice: (state, action) => {
      return {
        ...state,
        isLoading: false,
        error: action.payload,
      };
    },

    editStudentSuccessSlice: (state, action) => {
      const { payload } = action;
      state = state.filter((student) => student !== payload);
      return { ...state, isLoading: false, error: null };
    },

    editStudentFailedsSlice: (state, action) => {
      return { ...state, error: action.payload, isLoading: false };
    },

    deleteStudentSlice: (state, action) => {
      return { ...state, isLoading: true, error: null };
    },

    deleteStudentSuccessSlice: (state, action) => {
      return {
        ...state,
        isLoading: false,
        error: null,
        students: state.students.filter((i) => i.id !== action.payload),
      };
    },
    deleteStudentFailedSlice: (state, action) => {
      return {
        ...state,
        isLoading: false,
        error: action.payload,
      };
    },
  },
});

export const {
  fetchStudentsSlice,
  getStudentsFailedSlice,
  getStudentsSuccessSlice,
  //
  addStudentSlice,
  addStudentSuccessSlice,
  addStudentFailedSlice,
  //
  editStudentSuccessSlice,
  editStudentFailedSlice,
  //
  deleteStudentSlice,
  deleteStudentSuccessSlice,
  deleteStudentFailedSlice,
} = studentSlice.actions;

export default studentSlice.reducer;
