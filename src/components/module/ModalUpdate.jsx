import React from "react";
import { Button, Modal, Box } from "@mui/material";
import { closeModalSlice } from "../../redux/slice/modalSlice";
import { useDispatch, useSelector } from "react-redux";
import { Container } from "@mui/system";

import { TextField, Grid } from "@mui/material";
import { useForm } from "react-hook-form";
import { style } from "../material/Style";
import { Item } from "../material/Item";
import { updateModuleRedux } from "../../redux/actions/moduleActions";

function ModalUpdate({ isOpen }) {
  const dispatch = useDispatch();

  const moduleModal = useSelector((s) => s.modal);
  const { register, handleSubmit } = useForm();

  const onSubmit = (data) => {
    const module = {
      idModule: moduleModal.idModule,
      idStudent: moduleModal.idStudent,
      hours: data.hours,
      name: data.name,
    };
    dispatch(updateModuleRedux(module));
    dispatch(closeModalSlice());
  };

  return (
    <Modal
      hideBackdrop
      open={isOpen}
      aria-labelledby="child-modal-title"
      aria-describedby="child-modal-description"
    >
      <form onSubmit={handleSubmit(onSubmit)}>
        <Box sx={{ ...style, width: 450 }}>
          <Grid xs={15} item={true}>
            <Item>
              <Container sx={{ backgroundColor: "#ffff", padding: "10%" }}>
                <div>ID Module : {moduleModal.id}</div>
                <TextField
                  defaultValue={moduleModal.name}
                  fullWidth
                  size="small"
                  margin="normal"
                  id="standard-basic"
                  label="module name"
                  variant="outlined"
                  {...register("name")}
                />
                <TextField
                  type="number"
                  defaultValue={moduleModal.hours}
                  fullWidth
                  size="small"
                  margin="normal"
                  id="standard-basic"
                  label="hours"
                  variant="outlined"
                  {...register("hours")}
                />
              </Container>
            </Item>
          </Grid>
          <div
            style={{
              padding: "4% 0 0 25%",
            }}
          >
            <Button type="submit" variant="contained">
              update
            </Button>
            <Button
              onClick={() => dispatch(closeModalSlice())}
              variant="contained"
              color="secondary"
            >
              CLOSE
            </Button>
          </div>
        </Box>
      </form>
    </Modal>
  );
}

export default ModalUpdate;
