import { put, takeEvery } from "redux-saga/effects";
import {
  createModuleAPI,
  deleteModuleByIdAPI,
  getModulesAPI,
  updateModuleAPI,
} from "../../apis";
import {
  addModuleFailedSlice,
  addModuleSlice,
  addModuleSuccessSlice,
  deleteModuleFailedSlice,
  deleteModuleSlice,
  deleteModuleSuccesSlice,
  getModulesByIdStudentFailedSlice,
  getModulesByIdStudentSlice,
  getModulesByIdStudentSuccessSlice,
  updateModuleFailedSlice,
  updateModuleSlice,
  updateModuleSuccessSlice,
} from "../slice/studentSlice";
import types from "../types";

// update module by id
export function* getModulesByIdStudentSaga(action) {
  try {
    const { data } = yield getModulesAPI(action.payload);
    yield put(getModulesByIdStudentSlice());
    yield put(getModulesByIdStudentSuccessSlice(data));
  } catch (error) {
    error.response.data.message
      ? yield put(getModulesByIdStudentFailedSlice(error.response.data.message))
      : yield put(getModulesByIdStudentFailedSlice("error getting modules!!"));
  }
}
// update module
export function* updateModuleByIdSaga(action) {
  try {
    const { idStudent, idModule, name, hours } = action.payload;
    const module = {
      id: idModule,
      name,
      hours,
    };
    const response = yield updateModuleAPI(idStudent, module);
    yield put(updateModuleSlice());
    yield put(updateModuleSuccessSlice(module));
  } catch (error) {
    console.log("error:", error);
    error.response.data.message
      ? yield put(updateModuleFailedSlice(error.response.data.message))
      : yield put(updateModuleFailedSlice("error update module"));
  }
}
export function* addNewModuleSaga(action) {
  try {
    const { idStudent, module } = action.payload;
    const { data } = yield createModuleAPI(idStudent, module);
    const { id, name, hours } = data;
    const moduleSlice = {
      id,
      hours,
      name,
    };

    yield put(addModuleSlice());
    yield put(addModuleSuccessSlice(moduleSlice));
  } catch (error) {
    console.log("error: ", error);
    error.response.data.message
      ? yield put(addModuleFailedSlice(error.response.data.message))
      : yield put(addModuleFailedSlice("error adding module"));
  }
}

export function* deleteModuleByIdSaga(action) {
  try {
    const idModule = action.payload;
    const idStudent = action.payload;
    yield deleteModuleByIdAPI(idStudent, idModule);
    yield put(deleteModuleSlice());
    yield put(deleteModuleSuccesSlice(idModule));
  } catch (error) {
    console.log(error);
    error.response.data.message
      ? yield put(deleteModuleFailedSlice(error.response.data.message))
      : yield put(deleteModuleFailedSlice("error deleting module"));
  }
}

export function* watchModulesAsync() {
  yield takeEvery(types.GET_MODULES_BY_ID_STUDENT, getModulesByIdStudentSaga);
  yield takeEvery(types.CREATE_MODULE, addNewModuleSaga);
  yield takeEvery(types.UPDATE_MODULE, updateModuleByIdSaga);
  yield takeEvery(types.DELETE_MODULE_BY_ID, deleteModuleByIdSaga);
}
