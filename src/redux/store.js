import { configureStore } from "@reduxjs/toolkit";
import students from "./slice/studentsSlice";
import student from "./slice/studentSlice";
import modal from "./slice/modalSlice";
import createSagaMiddleware from "@redux-saga/core";
import { rootSaga } from "../redux/sagas";
const sagaMiddleware = createSagaMiddleware();
const store = configureStore({
 reducer: {
  students,
  modal,
  student,
 },
 middleware: (getDefaultMiddleware) =>
  getDefaultMiddleware({ thunk: false }).concat(sagaMiddleware),
});
sagaMiddleware.run(rootSaga);

export default store;
