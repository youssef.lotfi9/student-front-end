import { createSlice } from "@reduxjs/toolkit";

const modal = createSlice({
 name: "modal",
 initialState: {
  isOpen: false,
  idModule: 0,
  idStudent: 0,
  name: "",
  hours: "",
 },
 reducers: {
  openModalSlice: (state, action) => {
   return {
    ...state,
    isOpen: true,
    idModule: action.payload.idModule,
    idStudent: action.payload.idStudent,
    hours: action.payload.hours,
    name: action.payload.name,
   };
  },

  closeModalSlice: (state, action) => {
   return {
    ...state,
    isOpen: false,
    idStudent: null,
    idModule: null,
    name: "",
    hours: "",
   };
  },
 },
});

export const { openModalSlice, closeModalSlice } = modal.actions;
export default modal.reducer;
