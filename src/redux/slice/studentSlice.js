import { createSlice } from "@reduxjs/toolkit";

const student = createSlice({
  name: "student",
  initialState: {
    id: 0,
    fullName: "",
    branch: "",
    modules: [],
    error: null,
    isLoading: false,
  },
  reducers: {
    setStudentSlice: (state, action) => {
      const { payload } = action;

      return {
        ...state,
        id: payload.id,
        fullName: payload.fullName,
        branch: payload.branch,
        modules: payload.modules,
        isLoading: false,
      };
    },
    updateStudentSlice: (state, action) => {
      return { ...state, isLoading: true, error: null };
    },
    updateStudentSuccessSlice: (state, action) => {
      const { data } = action.payload;

      return {
        ...state,
        isLoading: false,
        fullName: data.fullName,
        branch: data.branch,
        id: data.id,
      };
    },
    updateStudentFailedSlice: (state, action) => {
      return { ...state, isLoading: false, error: action.payload };
    },
    addModuleSlice: (state, action) => {
      return { ...state, modules: [], isLoading: true, error: null };
    },
    addModuleSuccessSlice: (state, action) => {
      const { id, name, hours } = action.payload;

      return {
        ...state,
        modules: state.modules.concat({ id, name, hours }),
        isLoading: false,
        error: null,
      };
    },
    addModuleFailedSlice: (state, action) => {
      return { ...state, modules: [], isLoading: false, error: action.payload };
    },
    deleteModuleSlice: (state, action) => {
      return {
        ...state,
        isLoading: true,
        error: null,
      };
    },

    deleteModuleSuccesSlice: (state, action) => {
      const { idModule } = action.payload;
      return {
        ...state,
        isLoading: false,
        error: "",
        modules: state.modules.filter((m) => m.id !== idModule),
      };
    },

    deleteModuleFailedSlice: (state, action) => {
      return { ...state, isLoading: false, error: action.payload };
    },
    // update module slice
    updateModuleSlice: (state, action) => {
      return { ...state, isLoading: true, error: null };
    },

    updateModuleSuccessSlice: (state, action) => {
      let modules = [...state.modules];

      const index = modules.findIndex(
        (module) => module.id === action.payload.id
      );
      modules[index] = { ...modules[index], ...action.payload };
      return { ...state, modules, isLoading: false, error: null };
    },
    updateModuleFailedSlice: (state, action) => {
      return { ...state, isLoading: false, error: action.payload };
    },
    getModulesByIdStudentSlice: (state, action) => {
      return { ...state, isLoading: true, error: null };
    },
    getModulesByIdStudentSuccessSlice: (state, action) => {
      return {
        ...state,
        isLoading: false,
        error: null,
        modules: action.payload,
      };
    },

    getModulesByIdStudentFailedSlice: (state, action) => {
      return { ...state, isLoading: false, error: action.payload };
    },
    unsetStudent: (state, action) => {
      return {
        ...state,
        id: null,
        fullName: null,
        branch: null,
        modules: [],
        error: null,
        isLoading: false,
      };
    },
    unsetError: (state, action) => {
      return { ...state, error: null, isLoading: false };
    },
  },
});

export const {
  setStudentSlice,
  // update student
  updateStudentFailedSlice,
  updateStudentSlice,
  updateStudentSuccessSlice,

  // add module
  addModuleSlice,
  addModuleSuccessSlice,
  addModuleFailedSlice,
  // delete module
  deleteModuleSlice,
  deleteModuleFailedSlice,
  deleteModuleSuccesSlice,
  // get module
  getModulesByIdStudentSlice,
  getModulesByIdStudentSuccessSlice,
  getModulesByIdStudentFailedSlice,
  // update module
  updateModuleSlice,
  updateModuleSuccessSlice,
  updateModuleFailedSlice,

  // set unset
  unsetStudent,
  unsetError,
} = student.actions;
export default student.reducer;
