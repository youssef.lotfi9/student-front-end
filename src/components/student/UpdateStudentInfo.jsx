import * as React from "react";
import { Container } from "@mui/system";
import {
  Backdrop,
  Button,
  CircularProgress,
  Snackbar,
  Stack,
  TextField,
  MenuItem,
  Select,
  InputLabel,
  FormControl,
} from "@mui/material";
import { useForm, useController } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { updateStudentRedux } from "../../redux/actions/studentActions";
import { Alert } from "../material/Alert";
import { unsetError } from "../../redux/slice/studentSlice";

function UpdateStudentInfo() {
  const dispatch = useDispatch();
  const [updated, setUpdated] = React.useState(false);
  const currentStudent = useSelector((s) => s.student);
  const { error } = useSelector((s) => s.student);
  const { register, handleSubmit, control } = useForm({
    fullName: "",
    branch: "",
  });
  const onSubmit = (data) => {
    const { fullName, branch } = data;
    const upstudent = {
      id: currentStudent.id,
      fullName,
      branch,
    };
    dispatch(updateStudentRedux(upstudent));
    setUpdated(true);
  };

  // const { field } = useController({ name: "branch", control });
  const branchOptions = ["TDI", "TRI"];
  const handleSelectChange = (option) => {
    console.log("option.value", option.value);
    // field.onChange(option.value);
  };

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    dispatch(unsetError());
  };
  return (
    <>
      {!currentStudent.fullName ? (
        <Backdrop
          sx={{
            color: "#fff",
            zIndex: (theme) => theme.zIndex.drawer + 1,
          }}
          open={!currentStudent}
        >
          <CircularProgress color="inherit" />
        </Backdrop>
      ) : (
        <>
          <Stack spacing={3} sx={{ width: "100%" }}>
            <Snackbar open={error ? true : false} autoHideDuration={6000}>
              <Alert severity="error" sx={{ width: "100%" }}>
                {error}
              </Alert>
            </Snackbar>
          </Stack>
          <form onSubmit={handleSubmit(onSubmit)}>
            <Container
              sx={{
                backgroundColor: "#90cdec",
                padding: "2%",
                minWidth: "40px",
              }}
            >
              <TextField
                fullWidth
                size="small"
                margin="normal"
                id="standard-basic"
                label="Student name"
                variant="outlined"
                {...register("fullName")}
                defaultValue={currentStudent.fullName}
              />
              <FormControl
                fullWidth
                margin="normal"
                size="small"
                sx={{ minWidth: "70px" }}
              >
                <InputLabel id="demo-simple-select-label">Branch</InputLabel>
                <Select
                  defaultValue={currentStudent.branch}
                  {...register("branch")}
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  // value="TDI"
                  onChange={handleSelectChange}
                >
                  {branchOptions.map((g) => (
                    <MenuItem key={g} value={g}>
                      {g}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
              <div style={{ paddingTop: "10px" }}></div>

              <Button
                style={{ margin: "10px" }}
                variant="outlined"
                type="submit"
              >
                Update
              </Button>
            </Container>
          </form>
          {updated && !error ? (
            <Stack spacing={3} sx={{ width: "100%" }}>
              <Snackbar
                open={true}
                autoHideDuration={3000}
                onClose={handleClose}
              >
                <Alert
                  severity="success"
                  sx={{ width: "100%" }}
                  onClose={handleClose}
                >
                  student updated successfully
                </Alert>
              </Snackbar>
            </Stack>
          ) : (
            ""
          )}
        </>
      )}
    </>
  );
}

export default UpdateStudentInfo;
