import { all } from "redux-saga/effects";
import { watchStudentsAsync } from "./sagas/studentSaga";

export function* rootSaga() {
 yield all([watchStudentsAsync()]);
}
