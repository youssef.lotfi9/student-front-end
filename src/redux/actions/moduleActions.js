import types from "../types";

export const updateModuleRedux = ({ idModule, idStudent, hours, name }) => {
  return {
    type: types.UPDATE_MODULE,
    payload: {
      idModule,
      idStudent,
      hours,
      name,
    },
  };
};

export const deleteModuleByIdRedux = (idModule, idStudent) => {
  return {
    type: types.DELETE_MODULE_BY_ID,
    payload: {
      idModule: idModule,
      idStudent: idStudent,
    },
  };
};

export const getModulesRedux = (idStudent) => {
  return {
    type: types.GET_MODULES_BY_ID_STUDENT,
    payload: idStudent,
  };
};

export const createModuleRedux = (idStudent, module) => {
  return {
    type: types.CREATE_MODULE,
    payload: { idStudent, module },
  };
};
