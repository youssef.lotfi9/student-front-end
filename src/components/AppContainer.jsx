import React from "react";
import StudentList from "./student/StudentList";
import { useDispatch, useSelector } from "react-redux";

import { Grid } from "@mui/material";
import { Box, Container } from "@mui/system";
import AddStudent from "./student/AddStudent";

import { Item } from "./material/Item";
import { unsetStudent } from "../redux/slice/studentSlice";
import { getStudentsRedux } from "../redux/actions/studentActions";

function AppContainer() {
  const dispatch = useDispatch();
  const students = useSelector((s) => s.students.students);
  React.useEffect(() => {
    dispatch(getStudentsRedux());
    dispatch(unsetStudent());
  }, [dispatch]);

  return (
    <>
      <Box>
        <Grid container sx={{ backgroundImage: "" }}>
          <Item
            sx={{
              width: "90%",
              height: "50%",
              margin: 5,
              padding: "20px 0 20px 0",
              display: "block",
              marginLeft: "auto",
              marginRight: "auto",
            }}
          >
            <Grid>
              <Item
                sx={{
                  maxWidth: 900,
                  minWidth: 500,
                  height: "50%",
                  padding: "20px 0 20px 0",
                  display: "block",
                  marginLeft: "auto",
                  marginRight: "auto",
                  boxShadow: "none",
                }}
              >
                <AddStudent Item={Item} />
              </Item>
            </Grid>

            <Grid>
              <Item sx={{ boxShadow: "none" }}>
                <Container
                  sx={{
                    backgroundColor: "#90caf9",
                    padding: "20px 0 20px 0",
                  }}
                >
                  <StudentList students={students} Item={Item} />
                </Container>
              </Item>
            </Grid>
          </Item>
        </Grid>
      </Box>
    </>
  );
}

export default AppContainer;
