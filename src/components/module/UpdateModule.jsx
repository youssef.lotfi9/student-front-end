import React from "react";
import { Button, Box } from "@mui/material";
import { closeModalSlice } from "../../redux/slice/modalSlice";
import { useDispatch, useSelector } from "react-redux";
import { Container } from "@mui/system";
import { Item } from "../material/Item";
import { TextField, Grid } from "@mui/material";
import { useForm } from "react-hook-form";
import { style } from "../material/Style";

function UpdateModule({ isOpen }) {
  const dispatch = useDispatch();

  const studentModal = useSelector((s) => s.modal);

  const { register, handleSubmit } = useForm({
    defaultValues: {
      name: studentModal.name,
    },
  });

  const [branch, setbranch] = React.useState("");
  const onSubmit = (data) => {
    dispatch(closeModalSlice());
  };

  React.useEffect(() => {}, [branch]);
  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Box sx={{ ...style, width: 450 }}>
        <Grid xs={15} item={true}>
          <Item>
            <Container sx={{ backgroundColor: "#ffff", padding: "10%" }}>
              <div>ID Student : {studentModal.id}</div>
              <TextField
                defaultValue={studentModal.fullName}
                fullWidth
                size="small"
                margin="normal"
                id="standard-basic"
                label="name"
                variant="outlined"
                {...register("name")}
              />
              <TextField
                defaultValue={studentModal.fullName}
                fullWidth
                size="small"
                margin="normal"
                id="standard-basic"
                label="name"
                variant="outlined"
                {...register("hours")}
              />
            </Container>
          </Item>
        </Grid>
        <div
          style={{
            padding: "4% 0 0 25%",
          }}
        >
          <Button type="submit" variant="contained">
            update
          </Button>
          <Button
            onClick={() => dispatch(closeModalSlice())}
            variant="contained"
            color="secondary"
          >
            CLOSE
          </Button>
        </div>
      </Box>
    </form>
  );
}

export default UpdateModule;
