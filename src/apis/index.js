import axios from "axios";

axios.defaults.baseURL = "http://localhost:8090/api";

// students
export const getStudentsAPI = async (page = 0, size = 4) => {
  console.log("page: ", page, "\nsize: ", size);
  return await axios.get(`/students?page=${page}&size=${size}`);
};

export const getStudentByIdAPI = async (id) => {
  return await axios.get(`/students/${id}`);
};

export const createStudentAPI = async (student) => {
  return await axios.post(`/students`, student);
};

export const updateStudentAPI = async (student) => {
  return await axios.put(`/students/${student.id}`, student);
};

export const deleteStudentByIdAPI = async (id) => {
  return await axios.delete(`/students/${id}`);
};

// modules
export const getModulesAPI = async (idStudent) => {
  return await axios.get(`/students/${idStudent}/modules/`);
};

export const createModuleAPI = async (idStudent, modules) => {
  const mod = {
    id: null,
    name: modules.name,
    hours: modules.hours,
  };
  return await axios.post(`/students/${idStudent}/modules/`, mod);
};

export const updateModuleAPI = async (idStudent, module) => {
  return axios.put(`/students/${idStudent}/modules/${module.id}`, module);
};

export const deleteModuleByIdAPI = async ({ idStudent, idModule }) =>
  await axios.delete(`/students/${idStudent}/modules/${idModule}`);
