import * as React from "react";
import Box from "@mui/material/Box";
import Tab from "@mui/material/Tab";
import { TabContext } from "@mui/lab";
import TabList from "@mui/lab/TabList";
import TabPanel from "@mui/lab/TabPanel";
import UpdateStudentInfo from "./student/UpdateStudentInfo";
import ModulesList from "./module/ModulesList";
import { useDispatch, useSelector } from "react-redux";
import { Link, useParams } from "react-router-dom";
import { getStudentRedux } from "../redux/actions/studentActions";

export default function LabTabs() {
  const [value, setValue] = React.useState("1");
  const { idStudent } = useParams();
  const dispatch = useDispatch();
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  React.useEffect(() => {
    dispatch(getStudentRedux(idStudent));
  }, [dispatch]);

  const { isOpen } = useSelector((s) => s.modal);
  const student = useSelector((s) => s.student);
  return (
    <Box sx={{ width: "100%", typography: "body1" }}>
      <Link to={`/`}>HOME</Link>
      <TabContext value={value}>
        <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
          <TabList onChange={handleChange} aria-label="lab API tabs example">
            <Tab label="Update Student info" value="1" />
            <Tab label="Student's modules" value="2" />
          </TabList>
        </Box>
        <TabPanel value="1" index={0}>
          <UpdateStudentInfo />
        </TabPanel>
        <TabPanel value="2" index={1}>
          <ModulesList isOpen={isOpen} student={student} />
        </TabPanel>
      </TabContext>
    </Box>
  );
}
